import { React } from "react";
import data from "./ListData.js";
import "./List.css";

function List(props) {
  //create a new array by filtering the original array
  const filteredData = data.filter((el) => {
    //if no input the return the original
    if (props.input && props.index2 === "") {
      return el;
    }
    //return the item whichn contains the user input
    else {
      return (
        el.name.toLowerCase().includes(props.input.value) &&
        el.country.includes(props.input2)
      );
    }
  });
  return (
    <ul>
      {filteredData.map((item) => (
        <li key={item.id}>
          Location : {item.ville} <br /> Nombre : {item.nombre}
        </li>
      ))}
    </ul>
  );
}

export default List;
