import { useEffect } from "react";
import { useState } from "react";
import "./ListData.css";

/**
 * We're creating an array of 20 objects with the properties of id, name, description, and country
 * @param props - This is the props object that is passed to the component.
 * @returns An array of 20 objects with the properties of id, name, description, and country.
 */
const ListData = ({ personnes, input, index2, input2 }) => {
  /* Creating an empty array. */
  // eslint-disable-next-line
  const [SauvegardePerson, EditSauvegarde] = useState([...personnes]);
  const [filteredData, EditFilteredData] = useState([]);

  useEffect(() => {
    EditSauvegarde(personnes);
  }, [personnes]);

  useEffect(() => {
    EditFilteredData(
      personnes.filter(
        (el) =>
          input &&
          index2 !== "" &&
          el.name.toLowerCase().includes(input) &&
          el.country.includes(input2)
      )
    );
    // eslint-disable-next-line
  }, [input, input2]);
  /* Creating an array of 20 objects with the properties of id, name, description, and country. */

  // console.log(persons)
  /* It's looping through the array and printing the key and value. */

  return (
    <ul>
      {filteredData.length !== 0 &&
        filteredData.map((item) => (
          <li key={item.id}>
            Location : {item.name} <br /> Nombre : {item.country}
          </li>
        ))}
    </ul>
  );
};
/*
    
function List(props) {
  //create a new array by filtering the original array
  
  return (
    <ul>
      {filteredData.map((item) => (
        <li key={item.id}>
          Location : {item.name} <br /> Nombre : {item.country}
        </li>
      ))}
    </ul>
  );
}
*/

export default ListData;
