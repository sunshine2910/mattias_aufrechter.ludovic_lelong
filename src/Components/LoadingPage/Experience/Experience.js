import { React } from "react";
import "./Experience.css";

function Experiences() {
  return (
    <div>
      <div>
        <h2 className="titlepadding">Découvrez les expériences Airbnb</h2>
      </div>

      <div>
        <div className="contenantimage">
          <div>
            <div className="textparis">
              <h4>
                <strong></strong>
              </h4>
              <section className="texajustifé">
                <p>
                  Des expériences <br /> à vivre pendant <br /> votre voyage
                </p>
              </section>
              <a href="_" id="bouttonexperience">
                Expériences
              </a>
            </div>

            <div className="imagegrotte">
              <img
                src="https://a0.muscache.com/im/pictures/ae7fadb9-0b76-49a7-a45a-835a7390144e.jpg?im_w=480"
                alt="Photo_de_la_section_découvrez_les_expériences_airbnb"
                id="imagegrottetaille"
              />
            </div>
          </div>

          <div>
            <div className="textparis">
              <h4>
                <strong></strong>
              </h4>
              <section className="texajustifé">
                <p>
                  Des expériences <br /> à vivre <br /> de chez vous
                </p>
              </section>
              <a href="_" id="bouttonexperience">
                Expériences
              </a>
            </div>

            <div className="imagegrotte">
              <img
                src="https://a0.muscache.com/im/pictures/0fa7e0a7-c3c2-410a-b5c0-567cfca193cb.jpg?im_w=320"
                id="imagegrottetaille"
                alt="image_neutre"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Experiences;
