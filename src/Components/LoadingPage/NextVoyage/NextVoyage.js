import { React } from "react";
import "./NextVoyage.css";

function NextTravel() {
  return (
    <section>
      <div>
        <h2 className="titleH2">Des idées pour votre prochain voyage</h2>
      </div>

      <div className="centrercolumNextTravel">
        <div className="blocNextTraver1">
          <div className="imageNextTravel">
            <img
              src="https://a0.muscache.com/im/pictures/20b01752-1b6f-4390-9fc9-a2bc6f5fb643.jpg?im_w=240"
              alt="Photo_tour_eiffel_aibnb_section_des_idées_voyages_pour_votre_prochain_voyage"
            />
            <h4>
              <strong>Paris</strong>
            </h4>
            <p>À 2 kilomètres</p>
          </div>
        </div>

        <div className="blocNextTraver1">
          <div className="imageNextTravel">
            <img
              src="https://a0.muscache.com/im/pictures/06248faf-fafa-4adb-9447-3a393ca625a4.jpg?im_w=240"
              alt="Photo_tour_eiffel_aibnb_section_des_idées_voyages_pour_votre_prochain_voyage"
            />
            <h4>
              <strong>Les Sables-d'Olonne</strong>
            </h4>
            <p>À 403 kilomètres</p>
          </div>
        </div>

        <div className="blocNextTraver1">
          <div className="imageNextTravel">
            <img
              src="https://a0.muscache.com/im/pictures/3836379f-169d-4259-8c08-a6cb50461903.jpg?im_w=240"
              alt="Photo_tour_eiffel_aibnb_section_des_idées_voyages_pour_votre_prochain_voyage"
            />
            <h4>
              <strong>Nantes</strong>
            </h4>
            <p>À 341 kilomètres</p>
          </div>
        </div>

        <div className="blocNextTraver1">
          <div className="imageNextTravel">
            <img
              src="https://a0.muscache.com/im/pictures/2ce0594a-fe5c-4d88-b120-b91ff7456c1f.jpg?im_w=240"
              alt="Photo_tour_eiffel_aibnb_section_des_idées_voyages_pour_votre_prochain_voyage"
            />
            <h4>
              <strong>Quiberon</strong>
            </h4>
            <p>À 432 kilomètres</p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default NextTravel;
