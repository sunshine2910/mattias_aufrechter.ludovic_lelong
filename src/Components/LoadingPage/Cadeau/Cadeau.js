import { React } from "react";
import "./Cadeau.css";

function Cadeau() {
  return (
    <div className="flexbox">
      <div>
        <h2 className="titleCadeau"> Offrez une carte cadeau Airbnb </h2>
        <button type="button" className="Bouttonoffrez">
          En savoir plus
        </button>
      </div>

      <div>
        <img
          className="imagecarte"
          src="https://a0.muscache.com/im/pictures/1ca4a497-ba40-4429-be1c-ac6abe4209c6.jpg?im_w=2560"
          alt="Image_airbnb_partie_offrez_une_carte_cadeau_Airbnb"
        ></img>
      </div>
    </div>
  );
}

export default Cadeau;
