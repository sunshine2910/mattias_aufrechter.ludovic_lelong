// import List from "./Data/List.js";
import logo from "../image/Airbnb-logo.png";
import world from "../image/a-lechelle-mondiale.png";
import search from "../image/search_1.png";
import { useState } from "react";
import "./Header.css";
import { faker } from "@faker-js/faker";
import ListData from "../Components/Data/ListData";
import { useEffect } from "react";
function Header() {
  const personnes = [];
  const [persons, setPersons] = useState([]);
  const [inputText, setInputText] = useState("");
  const [inputNumber, setInputNumber] = useState("");

  const inputHandlerNumber = (e) => {
    const all = e.target.value;
    setInputNumber(all);
  };
  const inputHandler = (e) => {
    //convert input text to lower case
    const lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };
  function GenerationData() {
    for (let i = 0; i < 10; i++) {
      personnes.push({
        id: i,
        name: faker.name.firstName(),
        country: faker.address.country(),
      });
    }

    setPersons(personnes);
  }
  useEffect(() => {
    GenerationData();
    // eslint-disable-next-line
  }, []);
  return (
    <>
      <header className="App-header">
        <sections className="App-header-section">
          <img src={logo} className="App-logo" alt="logo" />
          <div className="barre_de_recherche">
            <div className="App-section-header-input">
              <h4>Destination</h4>
              <input placeholder="Ville" onChange={inputHandler} />
            </div>
            <div className="App-section-header-input">
              <h4>Voyageurs</h4>
              <input
                type="text"
                placeholder="Nombre"
                name="Voyageurs"
                onChange={inputHandlerNumber}
              />
            </div>
            <button type="submit">
              <img className="search" src={search} alt="search" />
            </button>
          </div>
          <img src={world} className="App-world" alt="world" />
        </sections>
        <ListData
          input={inputText}
          input2={inputNumber}
          personnes={[...persons]}
        />
      </header>
    </>
  );
}
export default Header;

/* <List input={inputText} input2={inputNumber} /> */
