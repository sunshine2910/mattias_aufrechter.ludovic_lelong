import "./App.css";
import React from "react";
import LandingPage from "./Page/LandingPage";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import FAQ from "./Page/FAQ";
import Err404 from "./Page/Err404";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/faq" component={FAQ} />
          <Route path="*" component={Err404} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
