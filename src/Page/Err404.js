import { useLocation, Link } from "react-router-dom";

function Err404() {
  let location = useLocation();

  return (
    <>
      <h1>404</h1>
      <code>Page : {location.pathname}</code>
      <br />
      <Link to="/">Home</Link>
    </>
  );
}
export default Err404;
