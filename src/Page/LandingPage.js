import Header from "../Components/Header";
import { Link } from "react-router-dom";
import NextTravel from "../Components/LoadingPage/NextVoyage/NextVoyage.js";
import Experiences from "../Components/LoadingPage/Experience/Experience.js";
import Cadeau from "../Components/LoadingPage/Cadeau/Cadeau.js";

function LandingPage() {
  return (
    <>
      <Header />
      <NextTravel />
      <Experiences />
      <Cadeau />
      <Link id="inputFaq" to="/FAQ">FAQ</Link>
    </>
  );
}

export default LandingPage;
